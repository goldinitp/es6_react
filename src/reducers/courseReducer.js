import * as types from '../actions/actionTypes';

export default function courseReducer(state = [], action) {
    // takes two parameters state(current state)
    // and action and returns new state
    switch(action.type) {
        // mutating state here not allowed in redux
        // case 'CREATE_COURSE':
        //     state.push(action.course);
        //     return state;
        case types.CREATE_COURSE:
            // console what it does
            return [...state,
                Object.assign({}, action.course)
            ];

        default:
            return state;
    }
}