import * as types from './actionTypes';

export function createCourse(course) {
    return { type: types.CREATE_COURSE, course};
    // if rhs matches lhs then write only one. ES6 feature
    // so write course and not course: course
}
